package com.dream.room.bill.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by MrTT (jiang.taojie@foxmail.com)
 * 2017/3/15.
 */
@Data
@AllArgsConstructor(staticName = "from")
@NoArgsConstructor
public class KeyValue {

    private String key;
    private Object value;

}
